/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */

/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const { join } = require('path');

const host = 'localhost';
const port = 3004;

module.exports = merge(common, {
  devtool: 'inline-source-map',
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://${host}:${port}`,
    './src/index.jsx',
  ],
  // Don't use hashes in dev mode for better performance
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    publicPath: '/',
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: [
        { loader: 'style-loader', options: { sourceMap: true } },
        { loader: 'css-loader', options: { sourceMap: true, importLoaders: 1 } },
        { loader: 'postcss-loader', options: { sourceMap: true } },
      ],
    }],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      __DEVELOPMENT__: true,
      __PRODUCTION__: false,
    }),
  ],
  devServer: {
    contentBase: join(__dirname, 'src', 'assets'),
    publicPath: '/',
    overlay: true,
    compress: true,
    host,
    port,
    hot: true,
    historyApiFallback: true,
    noInfo: true,
  },
});
