> Sympla challenge 

# Requirements

 - Install [pngquant][11] 
 - `npm i -g commitizen`
 - `npm i -g firebase-tools`

# Get Starter

**Starting the UI**
 - `npm install`
 - `npm start` *listen on port 3004*
 
**Starting Firebase Functions**
 - `cd functions && npm install && cd ..`
 - `firebase server -p 3005` 

## Features

 1. Create/list and remove events
 2. Create/edit/list and remove event ticket
 
### Stack

 * **Firebase Hosting** - [published by CI][0]
 * **Firebase Database** - using [re-base][9] and [valuelink][10]
 * **Firebase Storage** - save image (banner/logo) into storage
 * **CI/CD** - [using Gitlab Pipeline][1]
   - Create [Changelog on Gitlab][2]
 * **Google WebFont** - [with WebFont loader][3] 
 * **PostCSS** 
   - [BEM linter][4]
   - [CSSNext][5]
 * **Semantic Release**
   - Release Tag with [semantic-release-gitlab][6]
   - Prevent bad commits with [husky][7]
 * **GitFlow** Git approach that help-me with changelog and release versions.

[0]: https://sympla-events.ridermansb.me
[1]: https://gitlab.com/ridermansb/sympla-events/pipelines
[2]: https://gitlab.com/ridermansb/sympla-events/tags
[3]: https://github.com/typekit/webfontloader
[4]: https://github.com/postcss/postcss-bem-linter
[5]: http://cssnext.io/
[6]: https://www.npmjs.com/package/cz-conventional-changelog
[7]: https://www.npmjs.com/package/husky
[8]: https://danielkummer.github.io/git-flow-cheatsheet/
[9]: https://www.npmjs.com/package/re-base
[10]: https://github.com/Volicon/NestedLink
[11]: https://pngquant.org/
