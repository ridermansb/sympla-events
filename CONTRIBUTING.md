
# Deployment
 - [Use GitFlow][1]!
 - **Never** commit on master!
 
## How to develop a new feature?

 - `git flow feature start my-feature`
 - Implement your feature ..
 - `git flow feature finish my-feature`
 - Make a merge request from your branch
 
[1]: https://danielkummer.github.io/git-flow-cheatsheet/
