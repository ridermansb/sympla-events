/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */

/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define common configuration for Webpack
 * @see https://webpack.js.org/guides/production/
 * @type {webpack}
 */

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const { resolve } = require('path');

require('dotenv').config();

module.exports = {
  entry: {
    vendor: ['react', 'react-dom'],
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      FIREBASE_APIKEY: '',
      FIREBASE_AUTHDOMAIN: '',
      FIREBASE_DATABASEURL: '',
      FIREBASE_PROJECTID: '',
      FIREBASE_STORAGEBUCKET: '',
      FIREBASE_MESSAGINGSENDERID: '',
    }),
    new webpack.ProvidePlugin({
      // From http://madole.xyz/using-webpack-to-set-up-polyfills-in-your-site/
      Promise: 'imports-loader?this=>global!exports-loader?global.Promise!es6-promise',
      fetch: 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch',
    }),
    new HtmlWebpackPlugin({
      title: 'Sympla Events',
      template: resolve(__dirname, 'src', 'index.tpl.html'),
      chunksSortMode: 'dependency',
      minify: { collapseWhitespace: true },
    }),
    new FaviconsWebpackPlugin({
      logo: resolve(__dirname, 'favicon.png'),
      persistentCache: true,
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: true,
        coast: false,
        favicons: true,
        firefox: true,
        opengraph: false,
        twitter: true,
        yandex: false,
        windows: true,
      },
    }),
    // used to split out our sepcified vendor script
    // https://brotzky.co/blog/code-splitting-react-router-webpack-2/
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
      filename: '[name].[hash].js',
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'node-static',
      filename: 'node-static.js',
      minChunks(module) {
        const context = module.context;
        return context && context.indexOf('node_modules') >= 0;
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      async: 'used-twice',
      minChunks(module, count) {
        return count >= 2;
      },
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      assets: resolve(__dirname, 'src', 'assets'),
      components: resolve(__dirname, 'src', 'components'),
      pages: resolve(__dirname, 'src', 'pages'),
      api$: resolve(__dirname, 'src', 'api.js'),
      base$: resolve(__dirname, 'src', 'base.js'),
    },
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      exclude: /(node_modules|dist)/,
      enforce: 'pre',
      use: { loader: 'eslint-loader' },
    }, {
      test: /\.(html)$/,
      use: { loader: 'file-loader' },
      exclude: [resolve(__dirname, 'src', 'index.tpl.html')],
    }, {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: { loader: 'babel-loader' },
    }, {
      test: /\.(eot|woff|woff2|ttf|svg)$/,
      include: [resolve(__dirname, 'src', 'assets', 'fonts')],
      use: {
        loader: 'file-loader',
        query: { limit: 30000, name: '[name].[hash:8].[ext]', outputPath: 'assets/fonts/' },
      },
    }, {
      // From https://larsgraubner.com/webpack-images/
      test: /.*\.(gif|png|jpe?g|svg)$/i,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8000,
            name: '[name]_[sha512:hash:base64:7].[ext]',
          },
        },
        {
          loader: 'image-webpack-loader',
          options: {
            optipng: { optimizationLevel: 7 },
            pngquant: { quality: '65-90' },
            mozjpeg: { quality: 65 },
          },
        },
      ],
    }],
  },
};
