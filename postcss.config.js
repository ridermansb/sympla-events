/* eslint-disable import/no-extraneous-dependencies */

/**
 * Configure CSS
 */

const postcssImport = require('postcss-import');
const postcssCssnext = require('postcss-cssnext');
const postcssReporter = require('postcss-reporter');
const bemLinter = require('postcss-bem-linter');

module.exports = ({ options }) => ({
  map: options.map,
  parser: options.parser,
  plugins: [
    postcssImport(),
    postcssCssnext({ browsers: ['last 2 versions'] }),
    bemLinter(),
    postcssReporter(),
  ],
});
