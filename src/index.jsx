/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { render } from 'react-dom';
import WebFont from 'webfontloader';
import numeral from 'numeral';
import { BrowserRouter as Router } from 'react-router-dom';
// import createBrowserHistory from 'history/createBrowserHistory';
import App from './App';

// const customHistory = createBrowserHistory();

// load a locale
numeral.register('locale', 'pt-br', {
  delimiters: {
    thousands: '.',
    decimal: ',',
  },
  abbreviations: {
    thousand: 'mil',
    million: 'milhões',
    billion: 'b',
    trillion: 't',
  },
  ordinal() {
    return 'º';
  },
  currency: {
    symbol: 'R$',
  },
});
numeral.locale('pt-br');

WebFont.load({
  fontinactive(family) {
    if (family === 'Open Sans') {
      WebFont.load({
        families: ['Open Sans'],
        // eslint-disable-next-line import/no-webpack-loader-syntax,global-require
        urls: [require('file-loader!./assets/local-fonts.css')],
      });
    }
  },
  google: {
    families: ['Open Sans:Regular,Light,Bold'],
  },
});


const rootEl = document.getElementById('root');
render(<Router><App /></Router>, rootEl);

if (module.hot) {
  module.hot.accept('./App', () => {
    // eslint-disable-next-line global-require
    const NewRoot = require('./App').default;
    render(<Router><NewRoot /></Router>, rootEl);
  });
}
