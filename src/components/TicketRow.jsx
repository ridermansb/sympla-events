/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import numeral from 'numeral';
import Link from 'valuelink';

import editIcon from 'assets/images/ic-edit-2.png';
import editIcon2x from 'assets/images/ic-edit-2@2x.png';
import editIcon3x from 'assets/images/ic-edit-2@3x.png';

import deleteIcon from 'assets/images/ic-delete.png';
import deleteIcon2x from 'assets/images/ic-delete@2x.png';
import deleteIcon3x from 'assets/images/ic-delete@3x.png';

const TicketRow = ({ linkTicket, onDelete, onEdit }) => {
  const item = linkTicket.value;
  const preco = parseFloat(item.preco);
  const dataInicio = item.dataInicioVendas + item.horaInicioVendas;
  const dataFim = item.dataFimVendas + item.horaFimVendas;
  return (
    <tr className="grid">
      <td className="col-4">{item.tipo}</td>
      <td className="col-2">{moment(dataInicio, 'YYYY-MM-DDHH:mm').calendar()}</td>
      <td className="col-2">{moment(dataFim, 'YYYY-MM-DDHH:mm').calendar()}</td>
      <td className="col-1">{numeral(preco).format('$0.00')}</td>
      <td className="col-1">{item.quantidade}</td>
      <td className="col-1 table__row-actions">
        <button className="button button--icon" onClick={(e) => { e.preventDefault(); onEdit(linkTicket); }}>
          <img
            src={editIcon}
            srcSet={`${editIcon} 1x, ${editIcon2x} 2x, ${editIcon3x} 3x`}
            alt="Editar ticket"
          />
        </button>
        <button className="button button--icon" onClick={(e) => { e.preventDefault(); onDelete(linkTicket); }}>
          <img
            src={deleteIcon}
            srcSet={`${deleteIcon} 1x, ${deleteIcon2x} 2x, ${deleteIcon3x} 3x`}
            alt="Remover ticket"
          />
        </button>
      </td>
    </tr>
  );
};

TicketRow.displayName = 'TicketRow';
TicketRow.propTypes = {
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  linkTicket: PropTypes.instanceOf(Link).isRequired,
};

TicketRow.defaultProps = {
  onEdit: undefined,
  onDelete: undefined,
};

export default TicketRow;

