/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';

const NoData = ({
  text, icon, children, className,
}) => (
  <div className={`no-data ${className}`}>
    <div className="no-data__wrapper">
      {icon && <div className="no-data__icon">{icon}</div>}
      <div className="no-data__text">{text}</div>
      {children}
    </div>
  </div>
);

NoData.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  icon: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

NoData.defaultProps = {
  className: undefined,
  children: undefined,
  icon: undefined,
};

export default NoData;
