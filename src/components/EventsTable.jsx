/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import Link from 'valuelink';
import { autobind } from 'core-decorators';
import PropTypes from 'prop-types';
import { removeEvent } from 'api';
import EventRow from './EventRow';

export default class extends PureComponent {
  static displayName = 'EventsTable';

  static propTypes = {
    linkEvents: PropTypes.instanceOf(Link).isRequired,
  };

  state = {
  };


  @autobind
  // eslint-disable-next-line class-methods-use-this
  deleteTicket(linkEvent) {
    // eslint-disable-next-line no-restricted-globals,no-alert
    if (confirm(`Tem certeza que deseja remove o ticket ${linkEvent.value.titulo}?`)) {
      removeEvent(linkEvent.value);
    }
  }

  render() {
    const { linkEvents } = this.props;

    return (
      <div>
        <table className="table">
          <thead>
            <tr className="grid">
              <th className="col-5">EVENTO</th>
              <th className="col-2">DATA INÍCIO</th>
              <th className="col-2">DATA TERMINO</th>
              <th className="col-1">INGRESSOS</th>
              <th className="col-1" />
            </tr>
          </thead>
          <tbody>
            {linkEvents.map(it => (
              <EventRow
                key={`${it.key}`}
                linkEvent={it}
                onDelete={this.deleteTicket}
              />))
          }
          </tbody>
        </table>
      </div>);
  }
}
