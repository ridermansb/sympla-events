import React from 'react';
import PropTypes from 'prop-types';

const Avatar = ({ text }) => (
  <div className="avatar">
    {text}
  </div>
);

Avatar.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Avatar;
