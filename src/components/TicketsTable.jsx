/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import Link from 'valuelink';
import { autobind } from 'core-decorators';
import moment from 'moment';
import TicketFormModal from 'components/TicketFormModal';
import PropTypes from 'prop-types';
import TicketRow from './TicketRow';

const setModalTicketVisible = activeLinkTicket => state => ({ ...state, activeLinkTicket });

export default class extends PureComponent {
  static displayName = 'TicketsTable';

  static propTypes = {
    linkTickets: PropTypes.instanceOf(Link).isRequired,
  };

  state = {
    activeLinkTicket: false,
  };

  @autobind
  onModalTicketClose() {
    this.setState(setModalTicketVisible(undefined));
  }

  @autobind
  onModalTicketSubmitted(ticket) {
    const { linkTickets } = this.props;
    const { key, ...rest } = ticket;
    linkTickets.at(key).update(() => rest);
    this.setState(setModalTicketVisible(undefined));
  }

  @autobind
  editTicket(linkTicket) {
    this.setState(setModalTicketVisible(linkTicket));
  }
  @autobind
  deleteTicket(linkTicket) {
    const { linkTickets } = this.props;
    // eslint-disable-next-line no-restricted-globals,no-alert
    if (confirm(`Tem certeza que deseja remove o ticket ${linkTicket.value.tipo}?`)) {
      linkTickets.removeAt(linkTicket.key);
    }
  }

  render() {
    const { linkTickets } = this.props;
    const { activeLinkTicket } = this.state;

    return (
      <div>
        <table className="table">
          <thead>
            <tr className="grid">
              <th className="col-4">TIPO</th>
              <th className="col-2">DATA INÍCIO</th>
              <th className="col-2">DATA TERMINO</th>
              <th className="col-1">VALOR</th>
              <th className="col-1">QTDE.</th>
              <th className="col-1" />
            </tr>
          </thead>
          <tbody>
            {linkTickets.map(it => (
              <TicketRow
                key={`${it.key}`}
                linkTicket={it}
                onEdit={this.editTicket}
                onDelete={this.deleteTicket}
              />))
          }
          </tbody>
        </table>
        {activeLinkTicket &&
          <TicketFormModal
            onClose={this.onModalTicketClose}
            onSubmit={this.onModalTicketSubmitted}
            linkTicket={activeLinkTicket}
            eventStart={moment().format('YYYY-MM-DD')}
          />
        }
      </div>);
  }
}
