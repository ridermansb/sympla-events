/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import { autobind } from 'core-decorators';
import { NumberInput, Input } from 'valuelink/tags';
import PropTypes from 'prop-types';

export default class extends PureComponent {
  static displayName = 'FormField';

  static propTypes = {
    className: PropTypes.string,
    hint: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    style: PropTypes.object,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.arrayOf(PropTypes.node),
    ]),
  };

  static defaultProps = {
    children: undefined,
    hint: undefined,
    className: undefined,
    style: {},
  };

  state = {};

  @autobind
  handleNameInvalid(event) {
    this.validationField.innerText = event.target.validationMessage;
  }

  render() {
    const {
      children, hint, className, style, ...rest
    } = this.props;

    const Field = rest.type === 'number'
      ? NumberInput
      : Input;

    return (
      <div className={`form__field ${className}`} style={{ ...style }}>
        {children}
        <Field onInvalid={this.handleNameInvalid} {...rest} />
        <div className="form__field__meta">
          <span className="form__field__validation" ref={(el) => { this.validationField = el; }} />
          <span className="form__field__extra">{hint}</span>
        </div>
      </div>);
  }
}
