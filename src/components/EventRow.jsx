/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
import Link from 'valuelink';

import editIcon from 'assets/images/ic-edit-2.png';
import editIcon2x from 'assets/images/ic-edit-2@2x.png';
import editIcon3x from 'assets/images/ic-edit-2@3x.png';

import deleteIcon from 'assets/images/ic-delete.png';
import deleteIcon2x from 'assets/images/ic-delete@2x.png';
import deleteIcon3x from 'assets/images/ic-delete@3x.png';

const EventRow = ({ linkEvent, onDelete }) => {
  const item = linkEvent.value;
  const dataInicio = item.dataInicio + item.horaInicio;
  const dataFim = item.dataFim + item.horaFim;

  const totalTickets = item.tickets
    ? item.tickets.reduce((acc, next) => {
    // eslint-disable-next-line no-param-reassign
      acc += parseInt(next.quantidade, 10);
      return acc;
    }, 0)
    : 0;
  return (
    <tr className="grid">
      <td className="col-5">{item.nome}</td>
      <td className="col-2">{moment(dataInicio, 'YYYY-MM-DDHH:mm').calendar()}</td>
      <td className="col-2">{moment(dataFim, 'YYYY-MM-DDHH:mm').calendar()}</td>
      <td className="col-1">{totalTickets}</td>
      <td className="col-1 table__row-actions">
        <NavLink to={`/edit/${item.id}`} className="button button--icon">
          <img
            src={editIcon}
            srcSet={`${editIcon} 1x, ${editIcon2x} 2x, ${editIcon3x} 3x`}
            alt="Editar evento"
          />
        </NavLink>
        <button className="button button--icon" onClick={(e) => { e.preventDefault(); onDelete(linkEvent); }}>
          <img
            src={deleteIcon}
            srcSet={`${deleteIcon} 1x, ${deleteIcon2x} 2x, ${deleteIcon3x} 3x`}
            alt="Remover ticket"
          />
        </button>
      </td>
    </tr>
  );
};

EventRow.displayName = 'EventRow';
EventRow.propTypes = {
  onDelete: PropTypes.func,
  linkEvent: PropTypes.instanceOf(Link).isRequired,
};

EventRow.defaultProps = {
  onDelete: undefined,
};

export default EventRow;

