/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import { autobind } from 'core-decorators';
import PropTypes from 'prop-types';
import closeIcon from 'assets/images/ic-close.png';
import closeIcon2x from 'assets/images/ic-close@2x.png';
import closeIcon3x from 'assets/images/ic-close@3x.png';

export default class extends PureComponent {
  static displayName = 'Modal';
  static propTypes = {
    className: PropTypes.string,
    actionText: PropTypes.string,
    title: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.arrayOf(PropTypes.node),
    ]),
  };

  static defaultProps = {
    className: undefined,
    actionText: undefined,
    children: undefined,
  };

  @autobind
  closeModal(e) {
    e.preventDefault();
    const { onClose } = this.props;
    onClose();
  }

  render() {
    const {
      children, className, title, actionText,
    } = this.props;

    return (
      <div className={`modal ${className}`}>
        <div className="modal__content">
          <div className="modal__header">
            <h3>{title}</h3>
            <button className="button button--icon" onClick={this.closeModal}>
              <img
                src={closeIcon}
                srcSet={`${closeIcon} 1x, ${closeIcon2x} 2x, ${closeIcon3x} 3x`}
                alt="Fechar"
              />
            </button>
          </div>
          {children}
          <div className="modal__actions">
            <button className="button button--link" onClick={this.closeModal}>
              CANCELAR
            </button>
            <button className="button button--secondary" type="submit">
              {actionText || title}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
