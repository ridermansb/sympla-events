import React from 'react';
import PropTypes from 'prop-types';

const Breadcrumb = ({ title, children }) => (
  <div className="breadcrumb">
    <div className="grid grid--centered">
      <div className="container row">
        <div className="col-12">
          <h1 className="breadcrumb__title">
            {title}
          </h1>
          <span className="breadcrumb__extra">
            {children}
          </span>
        </div>
      </div>
    </div>
  </div>
);

Breadcrumb.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};
Breadcrumb.defaultProps = {
  children: undefined,
};

export default Breadcrumb;
