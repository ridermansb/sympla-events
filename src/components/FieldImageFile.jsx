/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React, { PureComponent } from 'react';
import { autobind } from 'core-decorators';
import cx from 'classnames';
import PropTypes from 'prop-types';
import calendarIconImage from 'assets/images/ic-image-placeholder.png';
import calendarIconImage2x from 'assets/images/ic-image-placeholder@2x.png';
import calendarIconImage3x from 'assets/images/ic-image-placeholder@3x.png';

export default class extends PureComponent {
  static displayName = 'FieldFile';

  static propTypes = {
    className: PropTypes.string,
    onFileSelected: PropTypes.func,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.element),
      PropTypes.arrayOf(PropTypes.node),
    ]),
  };

  static defaultProps = {
    className: undefined,
    onFileSelected: undefined,
    children: undefined,
  };

  state = {
    hasDragSupport: 'draggable' in document.createElement('span'),
    hasFileReaderSupport: window.File && window.FileList && window.FileReader,
  };

  // eslint-disable-next-line class-methods-use-this
  onDragOver(event) {
    event.preventDefault();
  }

  @autobind
  handleChange(event) {
    const { onFileSelected } = this.props;
    const { hasFileReaderSupport } = this.state;
    const files = event.target.files || event.dataTransfer.files;
    if (files.length <= 0) {
      return;
    }
    const file = files[0];

    if (onFileSelected) {
      onFileSelected(file);
    }

    this.setState({ file }, () => {
      if (!hasFileReaderSupport) {
        return;
      }
      const reader = new FileReader();
      const self = this;
      reader.onload = function setSrcImage(e) {
        self.imgTumb.src = e.target.result;
      };
      reader.readAsDataURL(file);
    });

    if (file.size > 2097152 /* 2mb */) {
      this.imageInput.setCustomValidity('Imagem deve ter no máximo 2mb');
    } else {
      this.imageInput.setCustomValidity('');
    }
    this.imageInput.checkValidity();
  }

  @autobind
  handleNameInvalid(event) {
    this.fieldValidation.innerText = event.target.validationMessage;
  }

  render() {
    const { hasDragSupport, file } = this.state;
    const {
      className, children, onFileSelected, ...rest
    } = this.props;

    const imageTumb = file
      ? <img src={calendarIconImage} alt={file.name} ref={(el) => { this.imgTumb = el; }} />
      : [
        <img
          key="preview"
          src={calendarIconImage}
          srcSet={`${calendarIconImage} 1x, ${calendarIconImage2x} 2x, ${calendarIconImage3x} 3x`}
          alt="Banner ou logo"
        />,
        <span key="instruction">Click {hasDragSupport
          ? 'ou arraste a imagem aqui'
          : 'para selecionar a imagem'}.
        </span>,
      ];

    const fieldFileClassName = cx('field-file', className, {
      'field-file--fit': file,
    });

    return (
      <div
        className={fieldFileClassName}
        draggable="true"
        onDragOver={this.onDragOver}
        onDrop={this.handleChange}
      >
        <label htmlFor="file">
          {imageTumb}
          {children}
          <input
            type="file"
            accept="image/*"
            name="files[]"
            id="file"
            onChange={this.handleChange}
            onInvalid={this.handleNameInvalid}
            ref={(el) => { this.imageInput = el; }}
            {...rest}
          />
          <span className="form__field__validation" ref={(el) => { this.fieldValidation = el; }} />
        </label>
      </div>);
  }
}
