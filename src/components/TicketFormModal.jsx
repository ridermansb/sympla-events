/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { autobind } from 'core-decorators';
import Link, { LinkedComponent } from 'valuelink';
import { Input } from 'valuelink/tags';
import moment from 'moment';
import Modal from 'components/Modal';
import FieldForm from 'components/FormField';
import dateIcon from 'assets/images/ic-date.png';
import dateIcon2x from 'assets/images/ic-date@2x.png';
import dateIcon3x from 'assets/images/ic-date@3x.png';
import timeIcon from 'assets/images/ic-time.png';
import timeIcon2x from 'assets/images/ic-time@2x.png';
import timeIcon3x from 'assets/images/ic-time@3x.png';

export default class extends LinkedComponent {
  static displayName = 'TicketForm';
  static propTypes = {
    onClose: PropTypes.func,
    onSubmit: PropTypes.func.isRequired,
    eventStart: PropTypes.string.isRequired,
    linkTicket: PropTypes.instanceOf(Link),
  };

  static defaultProps = {
    onClose: undefined,
    linkTicket: undefined,
  };

  state = {
    tipo: '',
    quantidade: null,
    preco: null,
    dataInicioVendas: undefined,
    horaInicioVendas: undefined,
    dataFimVendas: undefined,
    horaFimVendas: undefined,
    gratis: true,
  };

  componentWillMount() {
    const { linkTicket } = this.props;
    if (linkTicket) {
      this.setState({ ...linkTicket.value, key: linkTicket.key });
    }
  }

  @autobind
  onSubmitTicket(e) {
    const { onSubmit } = this.props;
    e.preventDefault();
    onSubmit(this.state);
  }

  @autobind
  closeModal() {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
  }

  render() {
    const { eventStart } = this.props;
    const { dataInicioVendas } = this.state;
    const linked = this.linkAll();

    return (
      <form onSubmit={this.onSubmitTicket}>
        <Modal title="CRIAR INGRESSO" onClose={this.closeModal}>
          <FieldForm
            autoFocus
            className="form__field--fluid form__field--required"
            type="text"
            placeholder="Tipo de ingreço"
            required="true"
            title="Campo obrigatório"
            valueLink={linked.tipo}
          />

          <FieldForm
            className="form__field--fluid form__field--required"
            type="number"
            placeholder="Quantidade"
            required="true"
            min="0"
            title="Campo obrigatório"
            valueLink={linked.quantidade}
          />

          <div className="grid">
            <FieldForm
              className="col-7 form__field--fluid"
              type="number"
              placeholder="Preço"
              valueLink={linked.preco}
              min="0"
              step="0.01"
              style={{ marginRight: 20 }}
            />
            <div className="form__field">
              <label htmlFor="freeCheckbox" className="field-checkbox">
                <Input
                  id="freeCheckbox"
                  type="checkbox"
                  name="freeCheckbox"
                  checkedLink={linked.gratis}
                /> Grátis
                <div className="field-checkbox__indicator" />
              </label>
            </div>
          </div>

          <div className="grid grid--justify form__field form__field--fit">
            <div className="col-5">
              <FieldForm
                id="dataInicioVendas"
                name="dataInicioVendas"
                className="ticket-form--datetime form__field--icon"
                type="date"
                pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                min={eventStart}
                valueLink={linked.dataInicioVendas}
              >
                <label htmlFor="dataInicioVendas">Início vendas</label>
                <img
                  src={dateIcon}
                  srcSet={`${dateIcon} 1x, ${dateIcon2x} 2x, ${dateIcon3x} 3x`}
                  alt="Data inicio vendas"
                />
              </FieldForm>
            </div>
            <div className="col-5">
              <FieldForm
                id="horaInicioVendas"
                name="horaInicioVendas"
                className="ticket-form--datetime form__field--icon"
                type="time"
                valueLink={linked.horaInicioVendas}
              >
                <label htmlFor="horaInicioVendas">Hora início</label>
                <img
                  src={timeIcon}
                  srcSet={`${timeIcon} 1x, ${timeIcon2x} 2x, ${timeIcon3x} 3x`}
                  alt="Hora inicio vendas"
                />
              </FieldForm>
            </div>
          </div>

          <div className="grid grid--justify form__field">
            <div className="col-5">
              <FieldForm
                id="dataFimVendas"
                name="dataFimVendas"
                className="ticket-form--datetime form__field--icon"
                type="date"
                pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                min={moment(dataInicioVendas).format('YYYY-MM-DD')}
                valueLink={linked.dataFimVendas}
              >
                <label htmlFor="dataFimVendas">Término Vendas</label>
                <img
                  src={dateIcon}
                  srcSet={`${dateIcon} 1x, ${dateIcon2x} 2x, ${dateIcon3x} 3x`}
                  alt="Data fim vendas"
                />
              </FieldForm>
            </div>
            <div className="col-5">
              <FieldForm
                id="horaFimVendas"
                name="horaFimVendas"
                className="ticket-form--datetime form__field--icon"
                type="time"
                valueLink={linked.horaFimVendas}
              >
                <label htmlFor="horaFimVendas">Hora Fim</label>
                <img
                  src={timeIcon}
                  srcSet={`${timeIcon} 1x, ${timeIcon2x} 2x, ${timeIcon3x} 3x`}
                  alt="Hora fim evento"
                />
              </FieldForm>
            </div>
          </div>
        </Modal>
      </form>
    );
  }
}
