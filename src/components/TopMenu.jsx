/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { NavLink } from 'react-router-dom';
import Avatar from 'components/Avatar';

export default () => (
  <nav className="menu">
    <ul className="menu--left">
      <li> <NavLink exact to="/" className="menu__item" activeClassName="menu__item--active">Meus Eventos</NavLink> </li>
      <li> <NavLink exact to="/new" className="menu__item" activeClassName="menu__item--active">+Criar Evento</NavLink> </li>
    </ul>
    <ul className="menu--center"><li /></ul>
    <ul className="menu--right">
      <li style={{ marginTop: '-5px' }}>
        <Avatar text="GF" />
      </li>
    </ul>
  </nav>);
