/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { autobind } from 'core-decorators';
import { LinkedComponent } from 'valuelink';
import { Input } from 'valuelink/tags';
import moment from 'moment';
import NoData from 'components/NoData';
import Breadcrumb from 'components/Breadcrumb';
import FieldForm from 'components/FormField';
import FieldImageFile from 'components/FieldImageFile';
import TicketFormModal from 'components/TicketFormModal';
import TicketsTable from 'components/TicketsTable';
import dateIcon from 'assets/images/ic-date.png';
import dateIcon2x from 'assets/images/ic-date@2x.png';
import dateIcon3x from 'assets/images/ic-date@3x.png';
import timeIcon from 'assets/images/ic-time.png';
import timeIcon2x from 'assets/images/ic-time@2x.png';
import timeIcon3x from 'assets/images/ic-time@3x.png';
import publishIcon from 'assets/images/ic-publish-white.png';
import publishIcon2x from 'assets/images/ic-publish-white@2x.png';
import publishIcon3x from 'assets/images/ic-publish-white@3x.png';
import ticketIcon from 'assets/images/ic-ticket-white.png';
import ticketIcon2x from 'assets/images/ic-ticket-white@2x.png';
import ticketIcon3x from 'assets/images/ic-ticket-white@3x.png';
import { saveEvent } from 'api';

const setCharLeftForName = charLeftForName => state => ({ ...state, charLeftForName });
const setIsSubmitting = isSubmitting => state => ({ ...state, isSubmitting });
const setNewTicketVisible = newTicketVisible => state => ({ ...state, newTicketVisible });

const MAX_CHAR_FOR_NAME = 100;
const currentDate = moment();

export default class extends LinkedComponent {
  static displayName = 'NewEventPage';

  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  state = {
    charLeftForName: MAX_CHAR_FOR_NAME,
    newTicketVisible: false,
    isSubmitting: false,

    eventData: {
      nome: '',
      dataInicio: undefined,
      horaInicio: undefined,
      dataFim: undefined,
      horaFim: undefined,
      image: false,
      file: undefined,
      tickets: [],
    },
  };

  @autobind
  onNewTicketClose() {
    this.setState(setNewTicketVisible(false));
  }

  @autobind
  onTicketSubmitted(ticket) {
    this.linkAt('eventData').at('tickets').push(ticket);
    this.setState(setNewTicketVisible(false));
  }

  @autobind
  handleNameChange(nameValue) {
    this.setState(setCharLeftForName(MAX_CHAR_FOR_NAME - nameValue.length));
  }

  @autobind
  async handleSubmit(e) {
    e.preventDefault();
    this.setState(setIsSubmitting(true));

    const { history } = this.props;
    const { eventData } = this.state;

    try {
      await saveEvent(eventData);
      history.push('/');
    } catch (error) {
      console.error(error);
    } finally {
      this.setState(setIsSubmitting(false));
    }
  }

  @autobind
  fileSelected(file) {
    const imageType = /^image\//;
    if (!imageType.test(file.type)) {
      return;
    }

    this.linkAt('eventData').at('file').set(file);
  }

  @autobind
  newTicket(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState(setNewTicketVisible(true));
    return false;
  }

  render() {
    const {
      charLeftForName, isSubmitting, newTicketVisible, dataInicio,
    } = this.state;
    const linked = this.linkAt('eventData');

    const ticketsLink = linked.at('tickets');
    const hasTickets = ticketsLink.value.length > 0;

    const NewTicketButton = ({ text = 'CRIAR INGREÇO', className }) => (
      <button className={`button button--secondary ${className}`} onClick={this.newTicket}>
        <img
          src={ticketIcon}
          srcSet={`${ticketIcon} 1x, ${ticketIcon2x} 2x, ${ticketIcon3x} 3x`}
          alt="Criar novo ingreço"
        />  {text}
      </button>);
    NewTicketButton.propTypes = {
      text: PropTypes.string,
      className: PropTypes.string,
    };
    NewTicketButton.defaultProps = {
      className: '',
      text: 'CRIAR INGREÇO',
    };

    return (
      <div className="page">
        <form className="form" onSubmit={this.handleSubmit}>
          <Breadcrumb title="Criar Evento">
            <button type="submit" className="button button--secondary button--smaller">
              <img
                src={publishIcon}
                srcSet={`${publishIcon} 1x, ${publishIcon2x} 2x, ${publishIcon3x} 3x`}
                alt="Publish"
              /> PUBLICAR EVENTO
            </button>
          </Breadcrumb>

          <div className="grid grid--centered">
            <div className="container">
              <div className="col-12">
                <div className="card">
                  <h2 className="card__title">1. Nome e imagem do evento</h2>
                  <FieldForm
                    className="form__field--fluid form__field--required"
                    type="text"
                    placeholder="Nome do evento"
                    required="true"
                    maxLength={MAX_CHAR_FOR_NAME}
                    title="Campo obrigatório"
                    hint={`${charLeftForName} caracteres restantes`}
                    valueLink={linked.at('nome').onChange(this.handleNameChange)}
                  />
                  <div className="form__field">
                    <span className="field-title">Inserir imagem</span>
                    <label htmlFor="bannerRadio" className="field-radio">
                      <Input
                        id="bannerRadio"
                        type="radio"
                        name="radio"
                        value="banner"
                        valueLink={linked.at('image')}
                      /> Banner
                      <div className="field-radio__indicator" />
                    </label>
                    <label htmlFor="logoRadio" className="field-radio">
                      <Input
                        id="logoRadio"
                        type="radio"
                        name="radio"
                        value="logo"
                        valueLink={linked.at('image')}
                      /> Logo
                      <div className="field-radio__indicator" />
                    </label>
                  </div>
                  <div className="form__field">
                    <div className="grid">
                      <div className="col-3">
                        <FieldImageFile
                          className="field-file--icon"
                          onFileSelected={this.fileSelected}
                        />
                      </div>
                      <div className="col-5 form__field__explain" style={{ marginLeft: 20 }}>
                        A imagem escolhida deve estar em formato JPG, GIF, ou PNG e ter no máximo
                        2MB. A dimensão recomendada é de 1200 x 444 pixels. Imagens com altura ou
                        largura diferentes destas podem ser redimensionadas.
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <h2 className="card__title">2. Quando?</h2>

                  <div className="grid grid--justify">
                    <div>
                      <FieldForm
                        id="dataInicio"
                        name="dataInicio"
                        className="form__field--required form__field--icon"
                        type="date"
                        pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                        required="true"
                        title="Campo obrigatório"
                        min={currentDate.format('YYYY-MM-DD')}
                        valueLink={linked.at('dataInicio')}
                      >
                        <label htmlFor="dataInicio">Data início</label>
                        <img
                          src={dateIcon}
                          srcSet={`${dateIcon} 1x, ${dateIcon2x} 2x, ${dateIcon3x} 3x`}
                          alt="Data inicio evento"
                        />
                      </FieldForm>
                    </div>
                    <div>
                      <FieldForm
                        id="horaInicio"
                        name="horaInicio"
                        className="form__field--required form__field--icon"
                        type="time"
                        required="true"
                        title="Campo obrigatório"
                        valueLink={linked.at('horaInicio')}
                      >
                        <label htmlFor="horaInicio">Hora início</label>
                        <img
                          src={timeIcon}
                          srcSet={`${timeIcon} 1x, ${timeIcon2x} 2x, ${timeIcon3x} 3x`}
                          alt="Hora inicio evento"
                        />
                      </FieldForm>
                    </div>
                    <div>
                      <FieldForm
                        id="dataFim"
                        name="dataFim"
                        className="form__field--required form__field--icon"
                        type="date"
                        pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}"
                        required="true"
                        title="Campo obrigatório"
                        min={dataInicio || undefined}
                        valueLink={linked.at('dataFim')}
                      >
                        <label htmlFor="dataFim">Data fim</label>
                        <img
                          src={dateIcon}
                          srcSet={`${dateIcon} 1x, ${dateIcon2x} 2x, ${dateIcon3x} 3x`}
                          alt="Data fim evento"
                        />
                      </FieldForm>
                    </div>
                    <div>
                      <FieldForm
                        id="horaFim"
                        name="horaFim"
                        className="form__field--required form__field--icon"
                        type="time"
                        required="true"
                        title="Campo obrigatório"
                        valueLink={linked.at('horaFim')}
                      >
                        <label htmlFor="horaFim">Hora fim</label>
                        <img
                          src={timeIcon}
                          srcSet={`${timeIcon} 1x, ${timeIcon2x} 2x, ${timeIcon3x} 3x`}
                          alt="Hora fim evento"
                        />
                      </FieldForm>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <h2 className="card__title">3. Ingressos</h2>
                  {hasTickets && [
                    <NewTicketButton key="button" text="CRIAR NOVO INGREÇO" className="card__action" />,
                    <TicketsTable key="table" linkTickets={ticketsLink} />,
                  ]}
                  {!hasTickets &&
                    <NoData
                      text="Você ainda não possui ingressos criados."
                      className="no-tickets"
                    >
                      <NewTicketButton />
                    </NoData>
                  }

                </div>
              </div>
            </div>
          </div>

          <div className="form__actions">
            <button type="submit" className="button button--secondary button--bigger" disabled={isSubmitting}>
              <img
                src={publishIcon}
                srcSet={`${publishIcon} 1x, ${publishIcon2x} 2x, ${publishIcon3x} 3x`}
                alt="Publicar evento"
              /> PUBLICAR EVENTO
            </button>
          </div>
        </form>

        {newTicketVisible &&
          <TicketFormModal
            onClose={this.onNewTicketClose}
            onSubmit={this.onTicketSubmitted}
            eventStart={dataInicio || currentDate.format('YYYY-MM-DD')}
          />
        }

      </div>);
  }
}
