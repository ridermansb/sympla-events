/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { LinkedComponent } from 'valuelink';
import Breadcrumb from 'components/Breadcrumb';
import NoData from 'components/NoData';
import { NavLink } from 'react-router-dom';
import calendarIcon from 'assets/images/icon-calendario.png';
import calendarIcon2x from 'assets/images/icon-calendario@2x.png';
import calendarIcon3x from 'assets/images/icon-calendario@3x.png';
import calendarIconWhite from 'assets/images/icon-calendario-white.png';
import calendarIconWhite2x from 'assets/images/icon-calendario-white@2x.png';
import calendarIconWhite3x from 'assets/images/icon-calendario-white@3x.png';
import EventsTable from 'components/EventsTable';
import base from 'base';

export default class extends LinkedComponent {
  static displayName = 'HomePage';

  state = {
    events: [],
  };

  componentDidMount() {
    this.eventsRef = base.syncState('events', {
      context: this,
      state: 'events',
      asArray: true,
    });
  }

  componentWillUnmount() {
    base.removeBinding(this.eventsRef);
  }

  render() {
    const { events } = this.state;
    const hasEvents = events.length > 0;

    const NewEventButton = ({ className }) => (
      <NavLink to="/new" className={`button button--secondary ${className}`}>
        <img
          src={calendarIconWhite}
          srcSet={`${calendarIconWhite} 1x, ${calendarIconWhite2x} 2x, ${calendarIconWhite3x} 3x`}
          alt="Calendar"
        /> CRIAR NOVO EVENTO
      </NavLink>
    );
    NewEventButton.propTypes = {
      className: PropTypes.string,
    };
    NewEventButton.defaultProps = {
      className: '',
    };

    const linkEvents = this.linkAt('events');

    return (
      <div className="page">
        <Breadcrumb title="Meus Eventos">
          {hasEvents && <NewEventButton key="button" className="button--smaller" />}
        </Breadcrumb>

        {!hasEvents &&
          <NoData
            text="Bem-vindo(a). Você ainda não possui eventos."
            className="no-events"
            icon={<img
              src={calendarIcon}
              srcSet={`${calendarIcon} 1x, ${calendarIcon2x} 2x, ${calendarIcon3x} 3x`}
              alt="Calendar Icon"
            />}
          >
            <NewEventButton />
          </NoData>
        }

        {hasEvents &&
          <div className="grid grid--centered">
            <div className="container">
              <div className="card">
                <h4 className="card__title">MINHA LISTA DE EVENTOS</h4>
                <EventsTable key="table" linkEvents={linkEvents} />
              </div>
            </div>
          </div>
        }
      </div>);
  }
}
