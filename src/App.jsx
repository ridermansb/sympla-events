/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import 'assets/style.css';
import HomePage from 'pages/HomePage';
import NewEventPage from 'pages/NewEventPage';
import TopMenu from 'components/TopMenu';

const App = withRouter(({ location }) => ([
  <TopMenu key="topMenu" />,
  <Switch location={location} key="routerSwitch">
    <Route exact path="/" component={HomePage} />
    <Route exact path="/new" component={NewEventPage} />
  </Switch>,
]));

export default App;
