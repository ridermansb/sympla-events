/*
 Ridermansb
 Copyright (C) 2017 Ridermansb

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import base, { storage } from 'base';

export function removeEvent(event) {
  storage.ref().child(event.key).delete();
  return base.remove(`events/${event.key}`);
}

export async function saveEvent(event) {
  const { file, ...data } = event;

  const eventFb = await base.push('events', { data });
  const fileStorage = await storage.ref().child(eventFb.key).put(file);
  return base.update(`events/${eventFb.key}`, {
    data: { file: fileStorage.downloadURL },
  });
}
