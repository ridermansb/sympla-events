module.exports = {
  env: {
    browser: true,
    es6: true,
    commonjs: true
  },
  parserOptions: {
    ecmaVersion: 7,
    sourceType: 'module',
    ecmaFeatures: {'jsx': true},
  },
  plugins: ['react', 'jsx-a11y']
};
